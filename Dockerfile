FROM ubuntu:18.04 AS builder
RUN apt update && apt install wget unzip -y
RUN wget https://www.tooplate.com/zip-templates/2121_wave_cafe.zip
RUN unzip 2121_wave_cafe.zip && cd 2121_wave_cafe && tar -czf cafe.tgz * && mv cafe.tgz /root/cafe.tgz

FROM ubuntu:18.04
RUN apt update && apt install apache2 git wget -y
COPY --from=builder /root/cafe.tgz /var/www/html/
RUN cd /var/www/html/ && tar xzf cafe.tgz
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
VOLUME /var/log/apache2
WORKDIR /var/www/html/
EXPOSE 80
